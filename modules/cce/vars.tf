## Region and Availability zone variables ##

variable "region" {
}

variable "number_of_azs" {
}

variable "availability_zone" {
}

variable "availability_zone1" {
}

variable "availability_zone2" {
}

## Network variables ##

variable "subnet_name" {
}

## Environment variables ##

variable "app_name" {
}

variable "environment" {
}

### CCE variables ###

variable "container_flavor_id"{
}

variable "container_cluster_type"{
}

variable "container_network_type"{
}

variable "container_network_cidr"{
}

variable "service_network_cidr"{
}

variable "container_cluster_version"{
}

