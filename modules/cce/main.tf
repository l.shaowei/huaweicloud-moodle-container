# Provider Huawei Cloud
terraform {
  required_providers {
    huaweicloud          = {
      source             = "huaweicloud/huaweicloud"
      version = "1.38.2"
    }
  }
}

data "huaweicloud_vpc" "vpc" {
  name                   = "${var.vpc_name}"
  }

data "huaweicloud_vpc_subnet" "subnet" {
  name                   = "${var.subnet_name}"
  }

resource "huaweicloud_cce_cluster" "cce_cluster_main" {
  name                   = "${var.cluster_name}"
  flavor_id              = "${var.cluster_flavor}"
  vpc_id                 = "${var.vpc_id}"
  subnet_id              = "${data.huaweicloud_vpc_subnet.subnet.id}"
  container_network_type = "${var.network_type}"
}

resource "huaweicloud_cce_node_pool" "cce_node_pool_main" {
  cluster_id               = var.cluster_id
  name                     = "testpool"
  os                       = "EulerOS 2.5"
  initial_node_count       = 2
  flavor_id                = "s3.large.4"
  availability_zone        = var.availability_zone
  key_pair                 = var.keypair
  scall_enable             = true
  min_node_count           = 1
  max_node_count           = 10
  scale_down_cooldown_time = 100
  priority                 = 1
  type                     = "vm"

  root_volume {
    size       = 40
    volumetype = "SAS"
  }
  data_volumes {
    size       = 100
    volumetype = "SAS"
  }
}

resource "huaweicloud_cce_addon" "addon_test" {
  cluster_id    = var.cluster_id
  template_name = "metrics-server"
  version       = "1.0.0"
}

resource "huaweicloud_cce_namespace" "test" {
  cluster_id = var.cluster_id
  name       = "test-namespace"
}

resource "huaweicloud_cce_pvc" "test" {
  cluster_id  = var.cluster_id
  namespace   = var.namespace
  name        = var.pvc_name
  annotations = {
    "everest.io/disk-volume-type" = "SSD"
  }
  storage_class_name = "csi-disk"
  access_modes = ["ReadWriteOnce"]
  storage = "10Gi"
}
