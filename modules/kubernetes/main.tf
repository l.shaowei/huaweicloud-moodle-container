# Provider Huawei Cloud
terraform {
  required_providers {
    huaweicloud          = {
      source             = "huaweicloud/huaweicloud"
      version = "1.38.2"
    }
  }
}

data "huaweicloud_vpc" "vpc" {
  name                   = "${var.vpc_name}"
  }

data "huaweicloud_vpc_subnet" "subnet" {
  name                   = "${var.subnet_name}"
  }

